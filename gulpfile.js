const config = require('./config');
const enablePartials = true;
const autoprefixer = require('gulp-autoprefixer');
const browsersync = require('browser-sync').create();
const csscomb = require('gulp-csscomb');
const cached = require('gulp-cached');
const cssnano = require('gulp-cssnano');
const del = require('del');
const fileinclude = require('gulp-file-include');
const gulp = require('gulp');
const gulpif = require('gulp-if');
const npmdist = require('gulp-npm-dist');
const postcss = require('gulp-postcss');
const runsequence = require('run-sequence');
const replace = require('gulp-replace');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const useref = require('gulp-useref-plus');
const wait = require('gulp-wait');
let cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const minify = require('gulp-minify');

const paths = {
  base:   {
    base:         {
      dir:    './'
    },
    node:         {
      dir:    'node_modules'
    },
    packageLock:  {
      files:  'package-lock.json'
    }
  },
  dist:   {
    base:   {
      dir:    'dist'
    },
    libs:   {
      dir:    'dist/assets/libs'
    }
  },
  src:    {
    base:   {
      dir:    'src',
      files:  'src/**/*'
    },
    css:    {
      dir:    'assets/css',
      files:  'assets/css/**/*'
    },
    html:   {
      dir:    'src',
      files:  'src/*.html',
    },
    js:     {
      dir:    'src/assets/js',
      files:  'src/assets/js/**/*'
    },
    partials:   {
      dir:    'src/partials',
      files:  'src/partials/**/*'
    },
    scss:   {
      dir:    'assets/scss',
      files:  'assets/scss/**/*',
      main:   'assets/scss/*.scss'
    },
    tmp:    {
      dir:    'src/.tmp',
      files:  'src/.tmp/**/*'
    }
  }
};

gulp.task('compress', function() {
  gulp.src(['dist/scripts/*.js'])
    .pipe(minify())
    .pipe(gulp.dest('dist/scripts/'))
});

gulp.task('sass', function() {
  gulp.src(paths.src.scss.main)
    //.pipe(wait(500))
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(postcss([require('postcss-flexbugs-fixes')]))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('dist/css'))
    .pipe(browsersync.reload({
      stream: true
    }));

});

gulp.task('fileinclude', function() {

  if ( enablePartials ) {
    gulp.src(paths.src.html.files)
      .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file',
        indent: true,
        context: config
      }))
      .pipe(gulp.dest(paths.src.tmp.dir))
      .pipe(browsersync.reload({
        stream: true
      }));
  } else {
    browsersync.reload();
  }
});

gulp.task('browsersync', function() {
  browsersync.init({
    server: {
      baseDir: [paths.src.tmp.dir, paths.src.base.dir, paths.base.base.dir]
    },
  })
});

gulp.task('watch', ['browsersync', 'sass', 'fileinclude', 'compress'], function() {
  gulp.watch(paths.src.js.files, browsersync.reload);
  gulp.watch(paths.src.scss.files, ['sass']);
});

gulp.task('default', function (callback) {
  runsequence(['sass', 'compress','browsersync', 'watch'],
    callback)
});