
particlesJS("particles-js", {"particles":{"number":{"value":50, "density":{"enable":false, "value_area":1000}}, "color":{"value":"#FFF"}, "shape":{"type":"circle", "stroke":{"width":0.5, "color":"#FFF"}, "polygon":{"nb_sides":3}, "image":{"src":"img/github.svg", "width":150, "height":150}}, "opacity":{"value":1, "random":true, "anim":{"enable":false, "speed":1, "opacity_min":0.1, "sync":false}}, "size":{"value":5, "random":true, "anim":{"enable":true, "speed":6, "size_min":0.1, "sync":false}}, "line_linked":{"enable":true, "distance":150, "color":"#FFF", "opacity":0.5, "width":1}, "move":{"enable":true, "speed":2, "direction":"none", "random":false, "straight":false, "out_mode":"out", "bounce":false, "attract":{"enable":false, "rotateX":600, "rotateY":1200}}}, "interactivity":{"detect_on":"canvas", "events":{"onhover":{"enable":true, "mode":"grab"}, "onclick":{"enable":true, "mode":"push"}, "resize":true}, "modes":{"grab":{"distance":400, "line_linked":{"opacity":1}}, "bubble":{"distance":400, "size":40, "duration":2, "opacity":8, "speed":3}, "repulse":{"distance":200, "duration":0.4}, "push":{"particles_nb":4}, "remove":{"particles_nb":2}}}, "retina_detect":true}); var count_particles, stats, update; stats = new Stats; stats.setMode(0); stats.domElement.style.position = 'absolute'; stats.domElement.style.left = '0px'; stats.domElement.style.top = '0px'; document.body.appendChild(stats.domElement); count_particles = document.querySelector('.js-count-particles'); update = function() { stats.begin(); stats.end(); if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) { count_particles.innerText = window.pJSDom[0].pJS.particles.array.length; } requestAnimationFrame(update); }; requestAnimationFrame(update); ;

document.addEventListener("touchstart", function(){}, true);

$(document).ready(function () {
    $(".phone-menu").click(function () {
        $(".nav-items").slideToggle();
        $(".nav-line").toggleClass("cover");
    });

    $(".half-box input[type=radio]").click(function () {
        var val = $('input[name="term"]:checked').data("price");
        $('.chosen').html(val);
        $('#ethPrice').show();
    });

    showMenu();
});

$(window).resize(function () {
    showMenu();
});

function showMenu() {
    var windowWidth = $(window).innerWidth();
    if (windowWidth < 991) {
        $('.phone-menu').show();
        $('.nav-items').hide();
    } else {
        $('.phone-menu').hide();
        $('.nav-items').show();
        $('.nav-line').removeClass('cover');
    }
}
;

$(document).ready(centerDiv);
$(window).resize(centerDiv);

function centerDiv() {
    var winHeight = $(document).innerHeight(),
            divHeight = $('.sign-up-holder').height();

    $('.sign-up-holder').css('marginTop', (winHeight - divHeight) / 2);
}

$(document).ready(centerDiv2);
$(window).resize(centerDiv2);
function centerDiv() {
    var winHeight = $(document).innerHeight(),
            divHeight = $('.sign-up-holder').height();
    $('.sign-up-holder').css('marginTop', (winHeight - divHeight) / 2);
}
function centerDiv2() {
    var winHeight = $(document).innerHeight(),
            divHeight = $('.splash-container').height();
    $('.splash-container').css('paddingTop', (winHeight - divHeight) / 2);
}
document.addEventListener("touchstart", function () {}, true);
