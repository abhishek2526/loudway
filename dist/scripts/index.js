var app = angular.module('loudwayApp', ["ngRoute"]);
const helpData = [
  {
    heading: 'Hosting Events',
    questions: ['How do I create an event?', 'How do I edit event information?', 'How do I manage event invitations?', 'How do I resend event invitations?', 'How do I promote my event?', 'How do I send my event guests reminders?', 'How can my guests check in to my event?', 'How do I view my events analytics?', 'How do I delete an event?'],
    link: '#!article1'
  },
  {
    heading: 'Account',
    questions: ['What are the account types?', 'What are the user types?', 'What happens when I delete my account?', 'What to do if my account was hacked?', 'What to do with unrecognized account logins or unrecognized devices?', 'Infomatic on In App Advertising', 'What is a secure password?', 'How is my personal information secured?', 'How is my payment information secured?', 'How do I track all payments (transactions) on LoudWay?'],
    link: '#!article1'
  },
  {
    heading: 'Event Ticketing',
    questions: ['How do I track ticket sales?', 'How do I create tickets for my event?', 'How do I assign tickets to my guests?', 'How can I manage my events tickets?', 'How can I manage ticket pricing for my event?'],
    link: '#!article1'
  },
  {
    heading: 'Company Overview',
    questions: ['How much if LoudWay?', 'How many developers does LoudWay have?', 'Are there any career opportunities for me at LoudWay?', 'Who can access LoudWay?'],
    link: '#!article1'
  },
  {
    heading: 'Using LoudWay',
    questions: ['What are groups in LoudWay?', 'Starter Guide to My Profile', 'Starter Guide to My Events', 'Starter Guide to Inbox', 'Starter Guide to Event Creator', 'Starter Guide to Event Management', 'Starter Guide to Event Promotion', 'Starter Guide to Earning Revenue from Events', 'Starter Guide to LoudWay for Business', 'Starter Guide to LoudWay for Personal', 'Starter Guide to LoudWay for Education', 'What are activities in LoudWay?', 'What are event passes in LoudWay?', 'How do I report issues or feedback on LoudWay?', 'Guide for Social Media 0Auth Providers'],
    link: '#!article1'
  },
  {
    heading: 'Attending Events',
    questions: ['How do I RSVP?', 'How do I unRSVP?', 'How does my calender work?', 'How do I find events?'],
    link: '#!article1'
  }
];

app.controller('helpPageCtrl', function($scope) {
  $scope.helpData = helpData;
  $scope.toggleArray = [true, true, true, true, true, true, true];
  $scope.num = new Array($scope.helpData.length).fill(3);
  $scope.toggleQuestions = function(flag, index) {
    if(flag){
      $scope.num[index] = $scope.helpData[index].questions.length;
    } else {
      $scope.num[index] = 3;
    }
    $scope.toggleArray[index] = !$scope.toggleArray[index];
  }
});

app.controller('homeAppCtrl', function($scope) {
  
  jQuery("span.text-change").typed({
      strings: ["Welcome to LoudWay!", "an Event Based Social Network.", "Discover Nearby Events!", "Create Events Quickly!", "Manage Events Precisly!", "Store Memories Forever!", ],
      typeSpeed: 50, // typing speed
      backSpeed: 20, // backspacing speed
      backDelay: 1000, // pause before backspacing
      callback: function() {
          $(this)
      }
  });
  
});


app.controller('comingSoonAppCtrl', function($scope) {
});

app.controller('jobPageCtrl', function($scope) {
  $scope.toggleArray = [true, true, true, true];
});

app.controller('docsPageCrtl', function($scope) {
  $scope.toggleArray = [true];
});

app.controller('csrJobController', function($scope) {
  const $spoilerList = $('#spoiler_list');
  $spoilerList.on('show.bs.collapse','.collapse', function() {
      $spoilerList.find('.collapse.in').collapse('hide');
  });
});

app.controller('loudwayAppCtrl', function($scope) {
  $scope.goToHome = function() {
    window.location.href = window.location.protocol + '//' + window.location.host;
  }
});


app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
  .when("/", {
    templateUrl : './views/home.html'
  })
  .when("/jobs", {
    templateUrl : "./views/jobs.html"
  })
  .when("/csr-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/ec-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/smm-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/de-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/uer-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/uid-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/dse-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/p-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/ccc-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/bdr-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/ppc-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/iti-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/fse-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/wdes-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/pd-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/wdev", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/rnmd", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/bse", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/rme-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/ie-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/bie-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/tw-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/ssec-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/fie-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/rdd-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/cm-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/bd-job", {
    templateUrl : "./views/csr-job.html"
  })
  .when("/sar-job", {
    templateUrl : "./views/sar-job.html"
  })
  .when("/prr-job", {
    templateUrl : "./views/prr-job.html"
  })
  .when("/help", {
    templateUrl : "/views/help.html"
  })
  .when("/guide1", {
    templateUrl : "/views/guide1.html"
  })
  .when("/docs", {
    templateUrl : "/views/docs.html"
  })
  .when("/apitu", {
    templateUrl : "/views/apitu.html"
  })
  .when("/tos", {
    templateUrl : "/views/tos.html"
  })
  .when("/cg", {
    templateUrl : "/views/cg.html"
  })
  .when("/doc", {
    templateUrl : "/views/doc.html"
  })
  .when("/pp", {
    templateUrl : "/views/pp.html"
  })
  .when("/cp", {
    templateUrl : "/views/cp.html"
  })
  .when("/dpa", {
    templateUrl : "/views/dpa.html"
  })
  .when("/tcp", {
    templateUrl : "/views/tcp.html"
  })
  .when("/ma", {
    templateUrl : "/views/ma.html"
  })
  .when("/orpr", {
    templateUrl : "/views/orpr.html"
  })
  .when("/company", {
    templateUrl : "/views/company.html"
  })
  .when("/article", {
    templateUrl : "/views/article.html"
  })
  .when("/coming-soon", {
    templateUrl : "/views/cs.html"
  })
  .when("/article1", {
    templateUrl : "/views/article1.html"
  })
  .otherwise({ redirectTo: '/coming-soon' });

  // $locationProvider.html5Mode(true);
}]);
